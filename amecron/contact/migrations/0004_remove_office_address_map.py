# -*- coding: utf-8 -*-
# Generated by Django 1.10.8 on 2017-11-12 18:05
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0003_auto_20171111_0328'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='office',
            name='address_map',
        ),
    ]
