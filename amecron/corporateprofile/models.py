from django.db import models
from modelcluster.fields import ParentalKey

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel
from wagtail.wagtailsnippets.edit_handlers import SnippetChooserPanel
from wagtail.wagtailsnippets.models import register_snippet

from amecron.utils.models import LinkFields, Preview, HeroImage, Sections


class Personnel(Preview, models.Model):
    facebook = models.ForeignKey(
        'SocialMediaLink', blank=True, null=True, on_delete=models.SET_NULL, related_name='+'
    )
    linkedin = models.ForeignKey(
        'SocialMediaLink', blank=True, null=True, on_delete=models.SET_NULL, related_name='+'
    )

    twitter = models.ForeignKey(
        'SocialMediaLink', blank=True, null=True, on_delete=models.SET_NULL, related_name='+'
    )

    panel = [
        SnippetChooserPanel('facebook'),
        SnippetChooserPanel('linkedin'),
        SnippetChooserPanel('twitter'),
    ] + Preview.panels


@register_snippet
class SocialMediaLink(LinkFields):
    title = models.CharField(max_length=255, help_text="Link title")
    icon = models.CharField(max_length=255, help_text="Social Media Icon", blank=True)

    panels = LinkFields.panels + [
        FieldPanel('title'),
        FieldPanel('icon'),
    ]

    def __str__(self):
        return '{}@{}'.format(self.title, self.link)


class FirmProfileSections(Orderable, Sections):
    page = ParentalKey("corporateprofile.FirmProfile", related_name="firm_profile_sections")


class FirmProfile(Page, HeroImage):
    intro = models.TextField()

    content_panels = Page.content_panels + [
        FieldPanel('intro'),

        MultiFieldPanel(
            [InlinePanel('firm_profile_sections')],
            heading="Firm Profile Sections",
            classname="collapsible"
        )
    ]

    promote_panels = Page.promote_panels + [
        MultiFieldPanel(
            HeroImage.panels,
            heading="Hero Image",
            classname="collapsible"
        )
    ]




