from django.apps import AppConfig


class CorporateProfileConfig(AppConfig):
    name = 'amecron.corporateprofile'

    def ready(self):
        pass
