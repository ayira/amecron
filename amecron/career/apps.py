from django.apps import AppConfig


class CareerConfig(AppConfig):
    name = 'amecron.career'
